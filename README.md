# ################################################################################

# Supplementary R Code for
# "Unbiased Recursive Partitioning Enables Robust and Reliable Outcome Prediction in Acute SCI"
 
# Muriel Buri, Lorenzo G. Tanadini, Armin Curt, Torsten Hothorn

##################################################################################

This repository a commented R-script as well as its output to reproduce
the results as well as plots of the manuscript "Unbiased Recursive Partitioning
Enables Robust and Reliable Outcome Prediction in Acute SCI".

Please refer to the manuscript for details on the models and simulation procedures.
